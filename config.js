var config = {};
//to log into the admin pannel
config.master_login={
    adminUser: "123",
    adminPswd: "123"
};

//for mysql connections
config.mysqldb = {
        host: 'localhost',
        user: 'yolo9',
        password: 'zaxscdVF',
        database: 'nuhoneypot'
};
// colors for charts on homepage
config.colors= {
    chart_colors: ["#D7ACAC", "#A6DEEE", "#E1E1A8", "#FF7373", "#FF7DFF"],
    oddColor: "#fafafa",
    evenColor: "#ffffff"
};
config.numLinesInTable=12;
module.exports = config;