var express = require('express');
var router = express.Router();
var mysql  = require('mysql');
var config = require('../config');

var pool = mysql.createPool({
    host: config.mysqldb.host,
    user: config.mysqldb.user,
    password: config.mysqldb.password,
    database: config.mysqldb.database
});

pool.getConnection(function (err, conn) {
    if (!err) {
        console.log("Database is connected ... \n\n");
        conn.release;
    } else {
        console.log("Error connecting database ... \n\n");
    }

});
var ajaxData=[];
var isAuthenticated = function (req, res ,next) {
    // if user is authenticated in the session, call the next() to call the next request handler
    // Passport adds this method to request object. A middleware is allowed to add properties to
    // request and response objects
    if (req.isAuthenticated()){
        return next();
    }else{
        // if the user is not authenticated then return error
        res.send('error - not loged in');
    }
 };
//
var getData = function(query,callback,req,res){
    pool.getConnection(function (err, conn) {
        if (!err) { // make sure we have DB connection
            conn.query(query, function (err, rows) {
                if (!err) {
                    var results=rows;
                    callback(rows,req,res);
                    conn.release();
                }
                else
                    return ('error');
            });
        }else{
            //TO DO  put in better place like check before even log in attempt -now bug message appears 3 times
            res.render('../', { message: req.flash('message','Error connecting to Database') });
        }
    });

}
//use callback because we need to wait until sql responds
var callback= function (data,req,res){
    res.send(data);
}



module.exports = function(passport) {
    //collect data to show in charts
    //list of contries
    router.get('/commonCountries', isAuthenticated, function(req, res) {
        getData('SELECT country, count(*) as total FROM sessions where country!="" && country !="0" group by country order by total desc limit '+config.numLinesInTable+';',callback,req,res);
    });
    //locations for heatmap
    router.get('/positionData', isAuthenticated, function(req, res) {
        getData('SELECT distinct s1.ip,s1.lon,s1.lat,s1.id from nuhoneypot.sessions s1 join nuhoneypot.sessions s2 on (s1.id!=s2.id and s1.lon=s2.lon and s1.lat=s2.lat) group by s1.lat order by s1.id desc limit '+config.numLinesInTable+';',callback,req,res);

    });

    //most common command line
    router.get('/commonCommandLines', isAuthenticated, function(req, res) {
        getData(' SELECT cmd, COUNT(*) AS total FROM cmds group by cmd order by total desc limit 12;', callback,req,res);
    });
    // first last date of data collection
    router.get('/dataCollectionDates', isAuthenticated, function(req, res) {
        getData('SELECT min(date),max(date) from sessions;', callback,req,res);
    });
    //
    router.get('/protocolData', isAuthenticated, function(req, res) {
        getData('SELECT protocol, COUNT(*) AS total FROM sessions GROUP BY protocol;',callback,req,res);
    });
    //protocol data last few days
    router.get('/protocolDataInterval', isAuthenticated, function(req, res) {
        getData('SELECT protocol,date, count(*) as total FROM sessions WHERE date >= DATE(NOW()) - INTERVAL 7 DAY group by DATE(date),protocol;', callback,req,res);
    });
    //total grouped by os
    router.get('/osData', isAuthenticated, function(req, res) {
        getData('SELECT p0f_os,count(*) AS total FROM sessions group by p0f_os;',callback,req,res);
    });
    //
    router.get('/osDataInterval', isAuthenticated, function(req, res) {
        getData('SELECT p0f_os,date, count(*) as total FROM sessions WHERE date >= DATE(NOW()) - INTERVAL 7 DAY group by DATE(date),p0f_os;', callback,req,res);
    });
    //
    router.get('/totalLoginAttemps', isAuthenticated, function(req, res) {
        getData('SELECT count(*) as total FROM nuhoneypot.users limit 1;',callback,req,res);
    });
    // most common passwords
    router.get('/commonPasswords', isAuthenticated, function(req, res) {
        getData('SELECT password, count(*) as total FROM nuhoneypot.users group by password order by total desc limit '+config.numLinesInTable+';',callback,req,res);
    });
    //most common usernames
    router.get('/commonUsernames', isAuthenticated, function(req, res) {
        getData('SELECT username, count(*) as total FROM nuhoneypot.users group by username order by total desc limit '+config.numLinesInTable+';',callback,req,res);
    });

    return router;
};