/**
 * Created by dina on 2015-12-01.
 */
$(function () {

    //create OS Chart
    //-------------
    //- PIE CHARTS-
    //-------------
    var createPie1=function(){
        colors['pop3']=colors[0];
        colors['telnet']=colors[1];
        $.getJSON('/nuData/protocolData', function(){})
            .done(function (data) {
               //console.log(JSON.stringify(data));
                var string="[";
                for(i=0;i<data.length;i++){
                    string=string+'{"value":"'+data[i].total+'","color":"'+ colors[data[i].protocol]+'","highlight":"'+colors[data[i].protocol]+'","label":"'+data[i].protocol+'"},';
                    $('#'+data[i].protocol+'Total').html(data[i].total);
                };
                string = string.slice(0,-1);
                var string=string+"]";
               // console.log(JSON.parse(string));
                PieData1= JSON.parse(string);
                //////////////
                // pie chart 1
                /////////////
                var pieChartCanvas1 = $("#pieChart1").get(0).getContext("2d");
                var pieChart1 = new Chart(pieChartCanvas1);
                pieChart1.Doughnut(PieData1, pieOptions);
            })
            .fail(function () {
                console.log('error');
            });
    }
    //create Protocol
    //////////////
    // pie chart 2
    /////////////
    var createPie2=function(){
        colors['null']=colors[0];
        colors['Windows']=colors[2];
        colors['Linux']=colors[1];
        colors['Mac OS X']=colors[3];
        var forLegend;

        $.getJSON('/nuData/osData', function(){})
            .done(function (data) {
                //console.log(JSON.stringify(data));
                var string="[";
                for(i=0;i<data.length;i++){
                    string=string+'{"value":"'+data[i].total+'","color":"'+colors[data[i].p0f_os]+'","highlight":"'+ colors[data[i].p0f_os]+'","label":"'+data[i].p0f_os+'"},';
                  if(data[i].p0f_os!=null){
                      forLegend=data[i].p0f_os;
                      forLegend=forLegend.replace(/\s+/g,'');// takeout spaces
                      $('#'+forLegend+'Total').html(data[i].total);
                  }else{
                      $('#otherTotal').html(data[i].total);
                  }


                };
                string = string.slice(0,-1);
                var string=string+"]";
                //console.log(JSON.parse(string));
                PieData1= JSON.parse(string);
                //////////////
                // pie chart 1
                /////////////
                var pieChartCanvas2 = $("#pieChart2").get(0).getContext("2d");
                var pieChart2 = new Chart(pieChartCanvas2);
                pieChart2.Doughnut(PieData1, pieOptions);
            })
            .fail(function () {
                console.log('error');
            });
    }



    var pieOptions = {
        //Boolean - Whether we should show a stroke on each segment
        segmentShowStroke: true,
        //String - The colour of each segment stroke
        segmentStrokeColor: "#fff",
        //Number - The width of each segment stroke
        segmentStrokeWidth: 2,
        //Number - The percentage of the chart that we cut out of the middle
        percentageInnerCutout: 50, // This is 0 for Pie charts
        //Number - Amount of animation steps
        animationSteps: 100,
        //String - Animation easing effect
        animationEasing: "easeOutBounce",
        //Boolean - Whether we animate the rotation of the Doughnut
        animateRotate: true,
        //Boolean - Whether we animate scaling the Doughnut from the centre
        animateScale: false,
        //Boolean - whether to make the chart responsive to window resizing
        responsive: true,
        // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
        maintainAspectRatio: true,
        //String - A legend template
        legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
    };
    createPie1();
    createPie2();
});
