/**
 * Created by dina on 2015-12-02.
 */
var  colors=["#D7ACAC","#A6DEEE","#E1E1A8","#FF7373","#FF7DFF"];

$(function () {
    var updateTotalLoginAttempts=function(){
        $.getJSON('/nuData/totalLoginAttemps', function(){})
            .done(function (data) {
                //console.log(JSON.stringify(data));
                var total=(data[0]['total']);
                $('#totalLogins').html(total);
            })
            .fail(function () {
                window.location.replace("/signout").die();
                console.log('error');
            });
    }

    var updateDataCollectionDates=function(){
        $.getJSON('/nuData/dataCollectionDates', function(){})
            .done(function (data) {
                //console.log(JSON.stringify(data));
                //to do get date functions and prettify the date
                var minDate=(data[0]['min(date)']);
                var maxDate=(data[0]['max(date)']);

                $('#dateDataMin').html(minDate);
                $('#dateDataMax').html(maxDate);
            })
            .fail(function () {
                console.log('error');
            });
    }

// call functions to update data
    updateTotalLoginAttempts();
    updateDataCollectionDates();
});

