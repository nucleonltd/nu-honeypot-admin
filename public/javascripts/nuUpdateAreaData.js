/**
 * Created by dina on 2015-12-02.
 */
//TODO refactor and scope vars

$(function () {
    //--------------
    //- AREA CHART -
    //--------------
    var areaChartOptions = {
        //Boolean - If we should show the scale at all
        showScale: true,
        //Boolean - Whether grid lines are shown across the chart
        scaleShowGridLines: true,
        //String - Colour of the grid lines
        scaleGridLineColor: "rgba(0,0,0,.05)",
        //Number - Width of the grid lines
        scaleGridLineWidth: 1,
        //Boolean - Whether to show horizontal lines (except X axis)
        scaleShowHorizontalLines: true,
        //Boolean - Whether to show vertical lines (except Y axis)
        scaleShowVerticalLines: true,
        //Boolean - Whether the line is curved between points
        bezierCurve: true,
        //Number - Tension of the bezier curve between points
        bezierCurveTension: 0.3,
        //Boolean - Whether to show a dot for each point
        pointDot: false,
        //Number - Radius of each point dot in pixels
        pointDotRadius: 4,
        //Number - Pixel width of point dot stroke
        pointDotStrokeWidth: 1,
        //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
        pointHitDetectionRadius: 20,
        //Boolean - Whether to show a stroke for datasets
        datasetStroke: true,
        //Number - Pixel width of dataset stroke
        datasetStrokeWidth: 2,
        //Boolean - Whether to fill the dataset with a color
        datasetFill: true,
        //String - A legend template
        legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
        //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
        maintainAspectRatio: true,
        //Boolean - whether to make the chart responsive to window resizing
        responsive: true
    };
    // Get context with jQuery - using jQuery's .get() method.
    var areaChartCanvas1 = $("#areaChart1").get(0).getContext("2d");
    // This will get the first returned node in the jQuery collection.
    var areaChart1 = new Chart(areaChartCanvas1);

    // Get context with jQuery - using jQuery's .get() method.
    var areaChartCanvas2 = $("#areaChart2").get(0).getContext("2d");
    // This will get the first returned node in the jQuery collection.
    var areaChart2 = new Chart(areaChartCanvas2);






    var addSortProtocols=function(string){
        // TODO make dynamic for any protocol
        var arr= new Array();
        var temp= new Array();
        if (string.indexOf("telnet") ==-1) {
            string=string+',telnet,0';
        };
        if (string.indexOf("pop3") ==-1) {
            string=string+',pop3,0';
        };
        arr=string.split(",");
        if(arr[1]=="telnet"){
            temp[0]=arr[0];//date
            temp[3]=arr[1];//telnet
            temp[4]=arr[2];//num-telnet
            temp[1]=arr[3];//pop3
            temp[2]=arr[4];//num-pop3
            return temp;
        }else{
            return arr;
        }


    };

    //

    var createArea1=function(){
        var string='[';
        var temp, tempDate,dates,protocol,total,tempLine,line;
        line='';
        temp='';
        var pop3= new Array();
        var telnet = new Array();

        $.getJSON('/nuData/protocolDataInterval', function(){})
            .done(function (data) {
               // console.log(JSON.stringify(data));
                for(i=0;i<data.length;i++) {
                    //console.log(data[i].date)
                    tempDate = data[i].date;
                    tempDate = new Date(tempDate);
                    tempDate = String(tempDate);
                    tempDate = tempDate.substr(4, 6);

                    protocol = data[i].protocol;
                    total = data[i].total;
                    if (tempDate != temp) {
                        string = string + '"' + tempDate + '",';
                        if (tempLine) { // make sure exists- not first time
                            tempLine = addSortProtocols(tempLine);
                            pop3.push(tempLine[2]);
                            telnet.push(tempLine[4]);
                        }
                        tempLine = tempDate;
                        temp = tempDate;
                    }
                    tempLine = tempLine + ',' + protocol + ',' + total;
                };
                    // catch last date --
                    tempLine=addSortProtocols(tempLine);
                    pop3.push(tempLine[2]);
                    telnet.push(tempLine[4]);
                    //
                    string = string.slice(0,-1);
                    string=string+']';
                    dates= JSON.parse(string);

                    var areaChartData1 = {
                        labels: dates,
                        datasets: [
                            {
                                label: "Telnet",
                                fillColor: colors[1],
                                strokeColor: colors[1],
                                pointColor: colors[1],
                                pointStrokeColor: colors[0],
                                pointHighlightFill: "#fff",
                                pointHighlightStroke: "rgba(220,220,220,1)",
                                data: telnet
                            },
                            {
                                label: "Pop3",
                                fillColor: colors[0],
                                strokeColor: colors[0],
                                pointColor:colors[0],
                                pointStrokeColor: "rgba(60,141,188,1)",
                                pointHighlightFill: "#fff",
                                pointHighlightStroke: "rgba(60,141,188,1)",
                                data:pop3
                            }
                        ]
                    };

                //Create the line chart
                areaChart1.Line(areaChartData1, areaChartOptions);
                totalTelnet=telnet.reduceRight(function(a,b){return Number(a)+Number(b);});
                $('#telnetWeekTotal').html(totalTelnet);
                totalPop3=pop3.reduceRight(function(a,b){return Number(a)+Number(b);});
                $('#pop3WeekTotal').html(totalPop3);
            })
            .fail(function () {
                console.log('error');
            });
    };

    var createArea2=function(){
        var string='[';
        var temp, tempDate,theDates,dates,os,total,tempLine,line,protocol;
        line='';
        temp='';
        string='';
        var osDataResults=[];

        var linux= new Array();
        var windows = new Array();
        var macOSX = new Array();
        var theNull = new Array();
        theDates='[';

        $.getJSON('/nuData/osDataInterval', function(){})
            .done(function (data) {
               // console.log(JSON.stringify(data));

                for(i=0;i<data.length;i++) {
                    //console.log(data[i].date)
                    tempDate = data[i].date;
                    tempDate = new Date(tempDate);
                    tempDate = String(tempDate);
                    tempDate = tempDate.substr(4, 6);
                    protocol = data[i].p0f_os;
                    total = data[i].total;
                    if (tempDate != temp) {// first time we see this date
                        theDates = theDates + '"' + tempDate + '",';
                        temp= tempDate;
                        osDataResults.push({"date":tempDate,"Windows":0,"Linux":0,"Mac OS X":0,"null":0});
                    }
                    for(var j=0;j<osDataResults.length;j++){
                        if(osDataResults[j].date==tempDate){
                            osDataResults[j][data[i].p0f_os]=data[i].total;
                        }
                    }

                };
                //console.log(osDataResults);
                for(var j=0;j<osDataResults.length;j++){
                    windows.push(osDataResults[j]['Windows']);
                    linux.push(osDataResults[j]['Linux']);
                    macOSX.push(osDataResults[j]['Mac OS X']);
                    theNull.push(osDataResults[j]['null']);
                }
                //console.log(macOSX)
                theDates = theDates.slice(0,-1);
                theDates=theDates+']';
                dates= JSON.parse(theDates);

                var areaChartData2 = {
                    labels: dates,
                    datasets: [
                        {
                            label: "Linux",
                            fillColor: colors[1],
                            strokeColor: colors[1],
                            pointColor: colors[1],
                            pointStrokeColor: colors[0],
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(220,220,220,.3)",
                            data: linux
                        },
                        {
                            label: "Unknown",
                            fillColor: colors[0],
                            strokeColor: colors[0],
                            pointColor:colors[0],
                            pointStrokeColor: "rgba(60,141,188,.3)",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(60,141,188,.3)",
                            data:theNull
                        },
                        {
                            label: "Max OS X",
                            fillColor: colors[2],
                            strokeColor: colors[2],
                            pointColor:colors[2],
                            pointStrokeColor: "rgba(60,141,188,.3)",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(60,141,188,.3)",
                            data:macOSX
                        },
                        {
                            label: "Windows",
                            fillColor: colors[3],
                            strokeColor: colors[3],
                            pointColor:colors[3],
                            pointStrokeColor: "rgba(60,141,188,.3)",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(60,141,188,.3)",
                            data:windows
                        }
                    ]
                };

                //Create the line chart
                areaChart2.Line(areaChartData2, areaChartOptions);
                //add up numbers in array
                if (windows.length>1){
                    totalWindows=windows.reduceRight(function(a,b){return Number(a)+Number(b);});
                }else{
                    totalWindows=0;
                }
                $('#windowsWeekTotal').html(totalWindows);

                if(linux.length>1){

                }

                totalLinux=linux.reduceRight(function(a,b){return Number(a)+Number(b);});
                $('#linuxWeekTotal').html(totalLinux);
                totalMacOSX=macOSX.reduceRight(function(a,b){return Number(a)+Number(b);});
                $('#macOSXWeekTotal').html(totalMacOSX);
                totalWindows=windows.reduceRight(function(a,b){return Number(a)+Number(b);});
                $('#windowsWeekTotal').html(totalWindows);
                totalOther=theNull.reduceRight(function(a,b){return Number(a)+Number(b);});
                $('#otherWeekTotal').html(totalOther);
            })
            .fail(function () {
                console.log('error');
            });
    };



    createArea1();
    createArea2();
});
