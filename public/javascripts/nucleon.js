/**
 * Created by Dina on 12/10/2015.
 */

$(document).ready(function() {


    /*
     The following snippet will watch for clicks on any link that points to an anchor and smoothly scroll down to it:
     */
    $('a[href^="#"]').on('click', function(event) {
        var target = $( $(this).attr('href') );
        if( target.length ) {
            event.preventDefault();
            $('html, body').animate({
                scrollTop: target.offset().top
            }, 1000);
        }
    });


});
