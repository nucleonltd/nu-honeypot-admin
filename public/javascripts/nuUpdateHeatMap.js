/**
 * Created by dina on 2015-12-02.
 */

$(function () {

    $.getJSON('./mapboxAccess.json', function(){})
        .done(function (data) {
            var mapboxAccessToken= data.mapboxAccessToken;

            L.mapbox.accessToken = mapboxAccessToken;


            var map = L.mapbox.map('map', 'mapbox.satellite')
                .setView([40, -74.50], 1);

            var myLayer = L.mapbox.featureLayer().addTo(map);

            // Set a custom icon on each marker based on feature properties.
            myLayer.on('layeradd', function(e) {
                var marker = e.layer,
                    feature = marker.feature;

                marker.setIcon(L.icon(feature.properties.icon));
            });


            var updatePositionData = function () {



                $.getJSON('/nuData/positionData', function () {
                    })
                    .done(function (data) {
                        // console.log(JSON.stringify(data));
                        var theData="[";
                        for(i=0;i<data.length;i++) {

                            theData=theData+'{'+
                                ' "type": "Feature",'+
                                ' "geometry": {'+
                                ' "type": "Point",'+
                                ' "coordinates": ['+
                                data[i].lon +','+data[i].lat+']'+
                                '},'+
                                '"properties": {'+
                                '"title": "'+data[i].ip+'",'+
                                '    "icon": {'+
                                '   "iconUrl": "/dist/img/heatmap-marker.png",'+
                                '        "iconSize": [12, 12],'+     // size of the icon
                                '       "iconAnchor": [10, 10],'+    // point of the icon which will correspond to marker's location
                                '        "popupAnchor": [0, -15],'+    // point from which the popup should open relative to the iconAnchor
                                '        "className": "dot"'+
                                '}'+
                                '}'+
                                '},'
                        }
                        theData=  theData.slice(0,-1);
                        theData=theData+']';
                        var geoJson= JSON.parse(theData);

                        // Add features to the map.
                        myLayer.setGeoJSON(geoJson);

                    })
                    .fail(function () {
                        console.log('error');
                    });

            }
            updatePositionData();
        })
        .fail(function () {
            console.log('error no map access token');
        });
});

