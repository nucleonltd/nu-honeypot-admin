$(function () {
    loadCommonCountries();
    loadCommonUsernames();
    loadCommonPasswords();
    loadCommonCommandLines();

});


var loadCommonCountries=function(){
    $.getJSON('/nuData/commonCountries', function(){})
        .done(function (data) {
            var thisCountry, thisCountryFlag;
            for(i=0;i<data.length;i++){
                thisCountry = data[i].country;
                thisCountry = thisCountry.toLowerCase();
                thisCountryFlag = 'flag-' + thisCountry;
                $('#Country'+i).find('.flag').addClass(thisCountryFlag);
                $('#Country'+i).find('.Country').html(countryCodes[data[i].country]);
                $('#Country'+i).find('.total').html(data[i].total);
            }
        })
        .fail(function () {
            console.log('error');
        });
}

var loadCommonUsernames=function(){
    $.getJSON('/nuData/commonUsernames', function(){})
        .done(function (data) {
            for(i=0;i<data.length;i++){
                if((data[i].username)== null)
                    username="<i>null</i>";
                else
                    username=data[i].username;
                $('#Username'+i).find('.Username').html(username);
                $('#Username'+i).find('.total').html(data[i].total);
            }
        })
        .fail(function () {
            console.log('error');
        });
}

var loadCommonPasswords=function(){
    $.getJSON('/nuData/commonPasswords', function(){})
        .done(function (data) {
            for(i=0;i<data.length;i++){
                if((data[i].password)== null)
                    username="<i>null</i>";
                else
                    password=data[i].password;
                $('#Password'+i).find('.Password').html(password);
                $('#Password'+i).find('.total').html(data[i].total);
            }
        })
        .fail(function () {
            console.log('error');
        });
}

var loadCommonCommandLines=function(){
    $.getJSON('/nuData/commonCommandLines', function(){})
        .done(function (data) {
            for(i=0;i<data.length;i++){
                $('#Command'+i).find('.Command').html(data[i].cmd);
                $('#Command'+i).find('.total').html(data[i].total);
            }
        })
        .fail(function () {
            console.log('error');
        });
}
