$(function () {
    var  colors=["#D7ACAC","#A6DEEE","#E1E1A8","#FF7373","#FF7DFF"];

    //-------------
    //- PIE CHARTS-
    //-------------

    var pieOptions = {
        //Boolean - Whether we should show a stroke on each segment
        segmentShowStroke: true,
        //String - The colour of each segment stroke
        segmentStrokeColor: "#fff",
        //Number - The width of each segment stroke
        segmentStrokeWidth: 2,
        //Number - The percentage of the chart that we cut out of the middle
        percentageInnerCutout: 50, // This is 0 for Pie charts
        //Number - Amount of animation steps
        animationSteps: 100,
        //String - Animation easing effect
        animationEasing: "easeOutBounce",
        //Boolean - Whether we animate the rotation of the Doughnut
        animateRotate: true,
        //Boolean - Whether we animate scaling the Doughnut from the centre
        animateScale: false,
        //Boolean - whether to make the chart responsive to window resizing
        responsive: true,
        // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
        maintainAspectRatio: true,
        //String - A legend template
        legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
    };
//////////////
// pie chart 1
/////////////
    var pieChartCanvas1 = $("#pieChart1").get(0).getContext("2d");
    var pieChart1 = new Chart(pieChartCanvas1);
    var obj = protocolData;
    var string="[";

    Object.keys(obj).forEach(function(key) {
        string=string+'{"value":"'+obj[key].total+'","color":"'+colors[key]+'","highlight":"'+ colors[key]+'","label":"'+obj[key].protocol+'"},';
    });
    string = string.slice(0,-1);
    var string=string+"]";
    var PieData1 = JSON.parse(string);
    pieChart1.Doughnut(PieData1, pieOptions);

//////////////
// pie chart 2
/////////////
    var pieChartCanvas2 = $("#pieChart2").get(0).getContext("2d");
    var pieChart2 = new Chart(pieChartCanvas2);
    var obj = osData;
    var string="[";

    Object.keys(obj).forEach(function(key) {
        string=string+'{"value":"'+obj[key].total+'","color":"'+colors[key]+'","highlight":"'+ colors[key]+'","label":"'+obj[key].p0f_os+'"},';
    });
    string = string.slice(0,-1);
    var string=string+"]";
    var PieData2 = JSON.parse(string);
    pieChart2.Pie(PieData2, pieOptions);

    //--------------
    //- AREA CHART -
    //--------------
    var areaChartOptions = {
        //Boolean - If we should show the scale at all
        showScale: true,
        //Boolean - Whether grid lines are shown across the chart
        scaleShowGridLines: true,
        //String - Colour of the grid lines
        scaleGridLineColor: "rgba(0,0,0,.05)",
        //Number - Width of the grid lines
        scaleGridLineWidth: 1,
        //Boolean - Whether to show horizontal lines (except X axis)
        scaleShowHorizontalLines: true,
        //Boolean - Whether to show vertical lines (except Y axis)
        scaleShowVerticalLines: true,
        //Boolean - Whether the line is curved between points
        bezierCurve: true,
        //Number - Tension of the bezier curve between points
        bezierCurveTension: 0.3,
        //Boolean - Whether to show a dot for each point
        pointDot: false,
        //Number - Radius of each point dot in pixels
        pointDotRadius: 4,
        //Number - Pixel width of point dot stroke
        pointDotStrokeWidth: 1,
        //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
        pointHitDetectionRadius: 20,
        //Boolean - Whether to show a stroke for datasets
        datasetStroke: true,
        //Number - Pixel width of dataset stroke
        datasetStrokeWidth: 2,
        //Boolean - Whether to fill the dataset with a color
        datasetFill: true,
        //String - A legend template
        legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
        //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
        maintainAspectRatio: true,
        //Boolean - whether to make the chart responsive to window resizing
        responsive: true
    };
    // Get context with jQuery - using jQuery's .get() method.
    var areaChartCanvas1 = $("#areaChart1").get(0).getContext("2d");
    // This will get the first returned node in the jQuery collection.
    var areaChart1 = new Chart(areaChartCanvas1);

    // Get context with jQuery - using jQuery's .get() method.
    var areaChartCanvas2 = $("#areaChart2").get(0).getContext("2d");
    // This will get the first returned node in the jQuery collection.
    var areaChart2 = new Chart(areaChartCanvas2);


    var string='[';
    var temp, tempDate,dates,protocol,total,tempLine,line;
    line='';
    var pop3= new Array();
    var telnet = new Array();



    var addSortProtocols=function(string){
        // TODO make dynamic for any protocol
        var arr= new Array();
        var temp= new Array();
        if (string.indexOf("telnet") ==-1) {
            string=string+',telnet,0';
        };
        if (string.indexOf("pop3") ==-1) {
            string=string+',pop3,0';
        };
       arr=string.split(",");
       if(arr[1]=="telnet"){
            temp[0]=arr[0];//date
            temp[3]=arr[1];//telnet
            temp[4]=arr[2];//num-telnet
            temp[1]=arr[3];//pop3
            temp[2]=arr[4];//num-pop3
            return temp;
       }else{
            return arr;
       }


    }

    var obj = protocolDateData;
    Object.keys(obj).forEach(function(key) {
        tempDate = obj[key].date;
        tempDate = new Date(tempDate);
        tempDate=String(tempDate);
        tempDate = tempDate.substr(4, 6);

        protocol= obj[key].protocol;
        total= obj[key].total;

        if(tempDate!=temp){
            string=string+'"'+tempDate+'",';
            if(tempLine){ // make sure exists- not first time
                tempLine=addSortProtocols(tempLine);
                pop3.push(tempLine[2]);
                telnet.push(tempLine[4]);
            }
            tempLine=tempDate;
            temp = tempDate;
        }
        tempLine=tempLine+','+protocol+','+total;
    });
    // catch last date --
    tempLine=addSortProtocols(tempLine);
    pop3.push(tempLine[2]);
    telnet.push(tempLine[4]);
    //
    string = string.slice(0,-1);
    string=string+']';
    dates= JSON.parse(string);

    var areaChartData1 = {
        labels: dates,
        datasets: [
            {
                label: "Telnet",
                fillColor: colors[1],
                strokeColor: colors[1],
                pointColor: colors[1],
                pointStrokeColor: colors[0],
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(220,220,220,1)",
                data: telnet
            },
            {
                label: "Pop3",
                fillColor: colors[0],
                strokeColor: colors[0],
                pointColor:colors[0],
                pointStrokeColor: "rgba(60,141,188,1)",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(60,141,188,1)",
                data:pop3
            }
        ]
    };

    var areaChartData2 = {
        labels: dates,
        datasets: [
            {
                label: "Unix",
                fillColor: "rgba(60,100,188,0.3)",
                strokeColor: "rgba(60,100,188,0.3)",
                pointColor: "rgba(60,100,188,0.3)",
                pointStrokeColor: "rgba(60,141,188,0.3)",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(220,220,220,1)",
                data: []
            },

            {
                label: "Other",
                fillColor: "rgba(255, 214, 222, 0.6)",
                strokeColor: "rgba(255, 214, 222, 0.6)",
                pointColor:"rgba(255, 214, 222,0.6)",
                pointStrokeColor:"rgba(210, 214, 222,1)",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(60,141,188,1)",
                data:[]
            },
            {
                label: "Max OS X",
                fillColor: "rgba(0,255,0,0.3)",
                strokeColor:"rgba(0,255,0,0.3)",
                pointColor:"rgba(0,255,0,0.3)",
                pointStrokeColor: "rgba(255,255,255,1)",
                pointHighlightFill: "#fff",
                pointHighlightStroke:  "rgba(255,255,255,1)",
                data:[]
            },
        ]
    };



    //Create the line chart
    areaChart1.Line(areaChartData1, areaChartOptions);
    totalTelnet=telnet.reduceRight(function(a,b){return Number(a)+Number(b);});
    $('#telnetWeekTotal').html(totalTelnet);
    totalPop3=pop3.reduceRight(function(a,b){return Number(a)+Number(b);});
    $('#pop3WeekTotal').html(totalPop3);

    areaChart2.Line(areaChartData2, areaChartOptions);



});