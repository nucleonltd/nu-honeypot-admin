var LocalStrategy   = require('passport-local').Strategy;
var config = require('../config');
var bCrypt = require('bcrypt-nodejs');

module.exports = function(passport){
	passport.use('login', new LocalStrategy({
            passReqToCallback : true
        },
        function(req, username, password, done) {

           if(username==config.master_login.adminUser&&password==config.master_login.adminPswd){
               // User and password both match, return user from done method
               // which will be treated like success
               return done(null, config.master_login.adminUser);
           }else{
               return done(null, false, req.flash('message', 'Invalid Password or Username combination')); // redirect back to login page
           }



        }))
};