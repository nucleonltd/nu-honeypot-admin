# GUI for NU-Honeypot

==============

##Prerequisite

 * nodejs
 * NU-Honeypot installed
 * For the world map with hotspots you need a mapbox access code (www.mapbox.com) edit the mapboxAccess.json, enter your access code token


##Steps to run the app

=====================

 * After cloning the repo, install the dependencies by running **npm install**
 * To start the server, run **npm start**


##Trouble Shooting - FAQ

================

Q. Why do I not see the world map in the hot spot chart?

A. Check that you have a valid access token to mapbox= http://www.mapbox

Update your tolken in the  mapboxAccess.json file


## Installing Prerequisits

=================
###Installing nodejs


####step 1 - install git(npm depends on it)

    $ sudo apt-get install git

####step 2 -Download and Extract nodeJS go to your home dir download nodejs from nodejs.org, create a node directory and extract the tar.gz contents into it

    $ cd ~
    $ wget https://nodejs.org/dist/v4.2.3/node-v4.2.3-linux-x64.tar.gz
    $ mkdir node
    $ tar xvf node-v*.tar.?z --strip-components=1 -C ./node

####step 3 - Configure npm's global prefix, create symbolic links to installed Node packages,(here  set it to /usr/local)

    $ mkdir node/etc
    $ echo 'prefix=/usr/local' > node/etc/npmrc

#### step 4 - move the node and npm binaries to our installation location and create symbolic links

    $ sudo mv node /opt/
    $ sudo ln -s /opt/node/bin/node /usr/local/bin/node
    $ sudo ln -s /opt/node/bin/npm /usr/local/bin/npm

### last step check installed by retrieving version

    $node -v

